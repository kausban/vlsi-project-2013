--Memory
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity memory is
    Port ( clk : in  STD_LOGIC;
           addr, addrVGA : in  STD_LOGIC_VECTOR (11 downto 0);
           dataIO : inout  STD_LOGIC_VECTOR (15 downto 0);
			  dataVGA : out  STD_LOGIC_VECTOR (7 downto 0);
           wren : in  STD_LOGIC;
           oe : in  STD_LOGIC);
end memory;

architecture Behavioral of memory is

	component memory_array is
		port ( clk : in  STD_LOGIC;
             address, addressVGA: in  STD_LOGIC_VECTOR (11 downto 0); 
             input  : in  STD_LOGIC_VECTOR (15 downto 0);
             output : out  STD_LOGIC_VECTOR (15 downto 0);
				 outputVGA : out  STD_LOGIC_VECTOR (7 downto 0);
             wren : in  STD_LOGIC);
	end component memory_array;
	
	signal outp_int  : std_logic_vector(15 downto 0);
	signal oe_int : std_logic;

begin

	mem : memory_array 
    port map ( clk  => clk,
               address => addr,
					addressVGA => addrVGA,
               input  => dataIO,
               output => outp_int,
					outputVGA => dataVGA,
               wren => wren );
					
	oe_reg: process(clk)
	begin
		if rising_edge(clk) then
			oe_int <= oe;
		end if;
	end process oe_reg;

	outp_ctrl: process(oe_int, outp_int, addr)
	begin
		if oe_int = '1' and addr /= X"FFF" then
			dataIO <= outp_int;
		else
			dataIO <= (others => 'Z');
		end if;
	end process outp_ctrl;

end Behavioral;

