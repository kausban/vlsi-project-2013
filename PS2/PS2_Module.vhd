----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:47:38 05/29/2013 
-- Design Name: 
-- Module Name:    PS2_Module - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PS2_Module is
port(
Clk 				: in std_logic;                       --System Clk
Reset 			: in std_logic;                       --System Reset
addr_bus		   : in std_logic_vector(11 downto 0);   --processor address bus
PS2_Clk 			: in std_logic;                       --Keyboard Clock line
PS2_Data 		: in std_logic;                       --Keyboard data line
IRQ 				: out std_logic;                      --Interrupt Signal
databus        : out std_logic_vector(15 downto 0);  --processor data bus
OE					: in std_logic); 						  --processor enable	
end PS2_Module;

----------------------------------------------
-- Behavioural model of PS2 Module
----------------------------------------------
architecture Behavioral of PS2_Module is
type STATES is (RST,STOP,LOAD);  
type reg_array IS ARRAY( 0 to 2) of std_logic_vector(7 downto 0); -- Registers for storing the scan codes.
signal reg : reg_array;
signal state :STATES:=RST;														-- States for keyboard check
signal data : std_logic_vector(7 downto 0):="00000000";				--Local variable for storing the 8 bit data

signal PS2_Sync,PS2_Data_tmp,startbit,parity_bit,stopbit:std_logic :='0'; --Local variable for storing the validity bit
signal parity :std_logic :='1';												--Local variable for parity bit calculation
signal count, count1 : integer :=0;											--count for taking eight bit data; count1: for storing the values in register.
signal tmp_clk,tmp_data :std_logic_vector (7 downto 0):="11111111"; --For clock debouncing
signal OE_PS2 :std_logic;
signal PS2_DataOutZ 	: std_logic_vector(7 downto 0);
begin
---------------------------------------------------------------------------
--This part of code synchronize the Master clock and keyboard clock PS2 clk
--And it is also taken care of debounce 
---------------------------------------------------------------------------- 
sync :process(Clk)
      begin
		if(rising_edge(Clk)) then 
		     	tmp_clk <= tmp_clk(6 downto 0) & PS2_Clk;
				if(tmp_clk = "11111111" ) then
					PS2_Sync <= '1';
				elsif(tmp_clk ="00000000") then
					PS2_Sync <= '0';
				end if;
				
				tmp_data <= tmp_data(6 downto 0) &PS2_Data;
				if(tmp_data = "11111111" ) then
					PS2_Data_tmp <= '1';
				elsif(tmp_data ="00000000") then
					PS2_Data_tmp <= '0';
				end if;
		end if; 
		end process;

------------------------------------------------------------------------------
-- This part of code takes care of reading the scan codes and sending data out
-----------------------------------------------------------------------------

Conversion : process(PS2_Sync,Reset,PS2_Data_tmp)
begin
if Reset = '1' then
	state <= RST;
	count<=0;
	reg<=(others=>"00000000");
	count1<=0;
elsif (falling_edge(PS2_Sync)) then        -- Check of falling edge of clock   
		case state is
			when RST =>
			   data<="00000000"; 
			   startbit <= PS2_Data_tmp;       --In RST state :Intialising al the variables
				state <= LOAD; 
				parity<='1';
				stopbit<='0';
			when LOAD =>
			  if(startbit ='0') then
				 if(count<8) then
				 data(count)<= PS2_Data_tmp;   --In Load state : All the eight bit data is stored as well as parity is calculated.
				 if(count>=1)then			
					parity <= parity xor data(count-1);
				 end if;
				 count<=count+1; 
				 else
						if(count=8) then
						parity_bit <=PS2_Data_tmp;
						parity <= parity xor data(count-1);
						state<=STOP;
						end if;
				 count<=0;
				 end if;
					if(count1=3) then          --The intialisation of count1 used for storing the value.
					count1<=0;
						reg	<=	(others=>"00000000");
					end if;
			 else
			 state<=RST;
			 end if;
		 when  STOP =>
		    if(parity_bit= parity) then     --In STOP state: Parity check is done, If it is valid then store the values in register.
						stopbit<=PS2_data_tmp;
				     	if(count1=0) then
						reg(count1)<=data;
						count1<=count1+1;
						elsif(count1=1 ) then  --the scan codes are stored for determing the ASCII code
						reg(count1) <=data;
						count1<=count1+1;
						elsif (count1=2) then
						reg(count1)<=data;
						count1<=count1+1;
						end if;	
			 else
						data<="00000000";
						count1<=0;
						reg <=(others=>"00000000");
			end if;
						state<=RST;	
		when others=>
             state<=RST;			 
		end case;    -- end of case statement
end if;            -- end of checking falling edge of PS2 clk
end process;
---------------------------------------------------------------------------------
--Below process detects the address bus 4095 and output enable sent by processor in response to IRQ
--If it is true then, the local variable OE_PS2 is set or reset.
----------------------------------------------------------------------------------

	oe_reg: process(Clk)
	begin
		if rising_edge(Clk) then
			OE_PS2 <= OE;
		end if;
	end process oe_reg;

	outp_ctrl: process(OE_PS2, PS2_DataOutZ, addr_bus)
	begin
		if OE_PS2 = '1' and addr_bus = X"FFF" then
			databus <= X"00" & PS2_DataOutZ;
		else
			databus <= (others => 'Z');
		end if;
	end process outp_ctrl;
	
	
--process(addr_bus,OE)
--begin
--   if(addr_bus(11 downto 0) ="111111111111" and OE='1') then
--    OE_PS2<='1';
--	 else
--	 OE_PS2<='0';
--	end if;
--end process;	
--
----------------------------------------------------------------------------------------------------
----As soon as the local variable is set then value of ASCII code is sent in lower 16 bit data bus.
-----------------------------------------------------------------------------------------------------
--
--process(OE_PS2, PS2_DataOutZ)
--begin
--if OE_PS2 = '1' then
--	databus <= X"00" & PS2_DataOutZ;
--else
--	databus <= (others=>'Z'); -- otherwise Z is sent in the data bus
--end if;
--end process;

--------------------------------------------
--Conversion of scan code to ASCII code        
--------------------------------------------

process(Clk,Reset)
begin
if(Reset='1') then
	PS2_DataOutZ<=(others=>'Z');
	IRQ<='0';
elsif(rising_edge(Clk)) then
	if(stopbit='1') then
		if(reg(0)=	X"70" and reg(1)=X"F0" and reg(2)=X"70") then
		  PS2_DataOutZ<=x"30"; --0
		  IRQ<='1';
		elsif (reg(0)=	X"69" and reg(1)=X"F0" and reg(2)=X"69") then
		  PS2_DataOutZ<=x"31"; --1
		  IRQ<='1';
		elsif (reg(0)=	X"72" and reg(1)=X"F0" and reg(2)=X"72") then
		  PS2_DataOutZ<=x"32";--2
		  IRQ<='1';			  
		elsif (reg(0)=	X"7A" and reg(1)=X"F0" and reg(2)=X"7A") then
		  PS2_DataOutZ<=x"33";--3
		  IRQ<='1';
		elsif (reg(0)=	X"6B" and reg(1)=X"F0" and reg(2)=X"6B") then
		  PS2_DataOutZ<=x"34";--4
		  IRQ<='1';
		elsif (reg(0)=	X"73" and reg(1)=X"F0" and reg(2)=X"73") then
		  PS2_DataOutZ<=x"35";--5
		  IRQ<='1';
		elsif (reg(0)=	X"74" and reg(1)=X"F0" and reg(2)=X"74") then
		  PS2_DataOutZ<=x"36";--6
		  IRQ<='1';
		elsif (reg(0)=	X"6C" and reg(1)=X"F0" and reg(2)=X"6C") then
		  PS2_DataOutZ<=x"37";--7
		  IRQ<='1';
		elsif (reg(0)=	X"75" and reg(1)=X"F0" and reg(2)=X"75")then
		  PS2_DataOutZ<=x"38";--8
		  IRQ<='1';			  
		elsif (reg(0)=	X"7D" and reg(1)=X"F0" and reg(2)=X"7D") then
		  PS2_DataOutZ<=x"39";--9
		  IRQ<='1';
		elsif (reg(0)=	X"7C" and reg(1)=X"F0" and reg(2)=X"7C") then
		  PS2_DataOutZ<=x"2A"; --*	
		  IRQ<='1';
		elsif (reg(0)=	X"7B" and reg(1)=X"F0" and reg(2)=X"7B") then
		  PS2_DataOutZ<=x"2D"; -- -
		  IRQ<='1';			  
		elsif (reg(0)=	X"79" and reg(1)=X"F0" and reg(2)=X"79") then
		  PS2_DataOutZ<=x"2B"; -- +
		  IRQ<='1';			  
		elsif (reg(0)=	X"4E" and reg(1)=X"F0" and reg(2)=X"4E") then
		  PS2_DataOutZ<=x"2F"; --/
		  IRQ<='1';
		elsif (reg(0)=	X"3E" and reg(1)=X"F0" and reg(2)=X"3E") then
		  PS2_DataOutZ<=x"28"; --(
		  IRQ<='1';			  
		elsif (reg(0)=	X"46" and reg(1)=X"F0" and reg(2)=X"46") then
		  PS2_DataOutZ<=x"29";--)	
		  IRQ<='1';
		 elsif (reg(0)=X"55" and reg(1)=X"F0" and reg(2)=X"55") then
		  PS2_DataOutZ<=x"3D";--=	
		  IRQ<='1';
		else
		  PS2_DataOutZ<=(others => 'Z');
		  IRQ<='0';
		end if;
	 else
		  PS2_DataOutZ<=(others => 'Z');
		  IRQ<='0';
	 end if;
end if;
end process;		 


end Behavioral; -- end of architecture  



