library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL ;

use work.fontROM.all;

entity VGAdescribe is
port (      clk                   : in std_logic;
            vsync, hsync, R, G, B         : out std_logic; 
				Data  : in  std_logic_vector(7 downto 0) ;
				Address  : out  std_logic_vector (11 downto 0));
end VGAdescribe;

architecture Behavioral of VGAdescribe is

	signal clock_div : std_logic;
	signal vcount : unsigned (18 downto 0) :="0000000000000000000" ;  
	signal hcount : unsigned (9 downto 0) :="0000000000" ;
	type line_type is array (0 to 639) of std_ulogic;
	signal line : line_type := (others => '0');
	signal pos : integer := 0;
	signal Init_Add : unsigned (11 downto 0) :=X"380"; --3b4
	signal Add_address : integer := 0;
	signal Add_count : integer := 0;
	signal line_offset : integer := 0;
	signal index : integer := 0;
	signal pixelLineNow : std_logic_vector(7 downto 0);
	
	signal aux: std_logic := '0';
	type STATES is (SEND, PAUSE, RECEIVE);
	signal state : STATES := SEND;
	
begin

	clock_divider : process (clk)  
   variable tmp_clk : std_logic := '0';
	begin
		if rising_edge(clk) then
			tmp_clk := not tmp_clk;
			clock_div <= tmp_clk;
		end if;
 	end process;
	
	signal_generator : process (clock_div)
		variable flag : integer := 0 ;
	begin
		if rising_edge(clock_div) then
			if (vcount >= 0 and vcount <= 8000) then --Front porch
				vsync <= '1' ;
			elsif (vcount >8000 and vcount <= 9600) then --Pulse width
				vsync <= '0' ;  
			elsif ( vcount >9600 and vcount <= 32800) then-- back porch
				vsync <= '1' ;
			elsif (vcount >32800 and vcount <=416800) then --display mode
				vsync <= '1' ;
			end if ;	
			
			if (vcount >=0 and vcount < 416800) then
				vcount <= vcount +1  ; --increment vcount at each clock
			elsif (vcount >= 416800) then 
				vcount <= "0000000000000000001";  -- make vcount zero when one cycle is completed
			end if ;		

			if (vcount = 8001 and flag =0  ) then
			-- this flag is used to detect falling edge of vcount and then used to synchronise hcount to vcount
				flag := 1 ;
			end if ;

			--creating hsync by counting hcount phases
			if ( hcount >= 0 and hcount <  16 and flag =1 ) then --Front porch
				hsync <= '1' ;
			elsif (hcount >= 16 and hcount < 112) then --Pulse width
				hsync <= '0' ;  
			elsif ( hcount >= 112 and hcount <  160) then-- back porch
				hsync <= '1' ;
			elsif (hcount >= 160 and hcount < 800) then--display mode
				hsync <= '1' ;
			end if ;
			
			if (hcount >= 0 and hcount < 800 and flag = 1) then
				hcount <= hcount +1  ; --increment hcount at each clock
			elsif (hcount >= 800) then 
				hcount <= "0000000001" ; -- make hcount zero when one cycle is completed
			end if ;
		end if;
	end process;

	image : process(clock_div)
	begin
		if rising_edge(clock_div) then
			R <= '0';
			B <= '0';
			G <= '0';
			if ((hcount >= 160) and (hcount < 800) and (vcount > 32800) and (vcount <= 416800)) then
				B <= line(pos);
				G <= line(pos);
				R <= line(pos);
				if(pos <639) then
					pos <= pos + 1;
				else
					pos <= 0;
					if line_offset < 2816 then
						line_offset <= line_offset + 256;
					else
						line_offset <= 0;
						if Add_address < 3120 then
							Add_address <= Add_address + 80;
						else
							Add_address <= 0;
						end if;
					end if;
				end if;
			end if;
		end if;
	end process;
	
	get_data : process(clk)
	begin
--		if ctrl_ext = '1' then
--			ctrl_memory <= '0';
--		end if;
		if rising_edge(clk) then
--			if ctrl_memory = '1' then
--				state <= SEND;
--				aux <= '0';
--				Add_count <= 0;
--			else
				case state is
					when SEND =>
						Address <= std_logic_vector (Init_Add  + Add_count + Add_address);
						if (Add_count < 79) then
							Add_count <= Add_count + 1;
						else
							Add_count <= 0;
						end if;
						if aux = '1' then
							for I in 0 to 7 loop
								line(index + I) <= pixelLineNow(I);
							end loop;
							if (index + 7) < 639 then
								index <= index + 8;
							else
								index <= 0;
								--ctrl_memory <= '1';
							end if;						
						end if;
						aux <= '0';
						state <= PAUSE;
					when PAUSE =>
						state <= RECEIVE;
					when RECEIVE =>
						pixelLineNow <= fontROM(to_integer(unsigned(Data)) + line_offset);
						aux <= '1';
						state <= SEND;
					when others =>
						null;
				end case;
			--end if;
		end if;
	end process;

end Behavioral;

