library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Project is
	Port ( res, clk : IN STD_LOGIC;
			 enLEDRes:	OUT	STD_LOGIC;
			 PS2_Clk, PS2_Data 		: in std_logic;
			 vsync, hsync, R, G, B         : OUT std_logic
		   );
end Project;

architecture Behavioral of Project is

	COMPONENT UKM910
   PORT(  oe	:	OUT	STD_LOGIC; 
          wren	:	OUT	STD_LOGIC; 		
          res	:	IN	STD_LOGIC; 
          clk	:	IN	STD_LOGIC; 
			 INTRPS2 : IN STD_LOGIC;
			 enLEDRes	:	OUT	STD_LOGIC;
          addressbus	:	OUT	STD_LOGIC_VECTOR (11 DOWNTO 0); 
          databus	:	INOUT	STD_LOGIC_VECTOR (15 DOWNTO 0));
   END COMPONENT;

	COMPONENT memory is
	PORT ( 	clk : in  STD_LOGIC;
				addr, addrVGA : in  STD_LOGIC_VECTOR (11 downto 0);
				dataIO : inout  STD_LOGIC_VECTOR (15 downto 0);
				dataVGA : out  STD_LOGIC_VECTOR (7 downto 0);
				wren : in  STD_LOGIC;
				oe : in  STD_LOGIC);
	END COMPONENT;
	
	COMPONENT VGAdescribe is
	PORT (      clk                   : in std_logic;
					vsync, hsync, R, G, B         : out std_logic;
					Data  : in  std_logic_vector(7 downto 0) ;
					Address  : out  std_logic_vector (11 downto 0));
	END COMPONENT;
	
	COMPONENT PS2_Module is
	PORT(
			Clk 				: in std_logic;                       --System Clk
			Reset 			: in std_logic;                       --System Reset
			addr_bus		   : in std_logic_vector(11 downto 0);   --processor address bus
			PS2_Clk 			: in std_logic;                       --Keyboard Clock line
			PS2_Data 		: in std_logic;                       --Keyboard data line
			IRQ 				: out std_logic;                      --Interrupt Signal
			databus        : out std_logic_vector(15 downto 0);  --processor data bus
			OE					: in std_logic); 						  --processor enable	
	END COMPONENT;

	signal oe, wren : STD_LOGIC := '0';
	signal INTRPS2 : STD_LOGIC := '0';
	signal addressbus, addrVGA	:	STD_LOGIC_VECTOR (11 DOWNTO 0) := (others => '0');
	signal databus	:	STD_LOGIC_VECTOR (15 DOWNTO 0) := (others => '0');
	signal dataVGA	:	STD_LOGIC_VECTOR (7 DOWNTO 0) := (others => '0');

begin

	comp_processor: UKM910 PORT MAP(
		oe => oe, 
		wren => wren, 
		res => res, 
		clk => clk, 
		INTRPS2 => INTRPS2,
		enLEDRes => enLEDRes,
		addressbus => addressbus, 
		databus => databus
   );

	comp_memory: memory PORT MAP(
		clk => clk,
		addr => addressbus,
		addrVGA => addrVGA,		
		dataIO => databus,
		dataVGA => dataVGA,
		wren => wren,
		oe => oe  
   );

	comp_VGA: VGAdescribe PORT MAP(
		clk => clk,
		vsync => vsync,
		hsync => hsync,
		R => R,
		B => B,
		G => G,
		Data => dataVGA,
		Address => addrVGA
   );
	
	comp_ps2 : PS2_Module PORT MAP(
		Clk => clk,
		Reset => res,
		addr_bus => addressbus,
		PS2_Clk => PS2_Clk,
		PS2_Data => PS2_Data,
		IRQ => INTRPS2,
		databus => databus,
		OE => oe
	);

end Behavioral;

