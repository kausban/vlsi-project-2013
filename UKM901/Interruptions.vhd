library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Interruptions is
	Port ( input	:	IN	STD_LOGIC_VECTOR (11 DOWNTO 0); 	--Data input		
          res, clk, enREGS :	IN	STD_LOGIC; 					--Reset, clock, enable, signal from PS2 controller, signal from control
			 INTRPS2, GIEstate : IN	STD_LOGIC; 					--Input from keyboard and from controller
			 inSelRegs : IN STD_LOGIC_VECTOR (2 DOWNTO 0);	--'Register to be outputed or updated'
			 enINTR :	OUT	STD_LOGIC; 							--Output bit in case of interruption
			 handledIntrp : OUT STD_LOGIC_VECTOR (2 DOWNTO 0);	--Interruption being handled
			 outREGS  : OUT STD_LOGIC_VECTOR (15 DOWNTO 0));--Output for the IEN & IFLAGS registers
end Interruptions;

architecture Behavioral of Interruptions is
	signal IFLAGS, IEN : std_logic_vector(11 downto 0) := (others => '0');			--IEN & IFLAGS registers
	signal ps2int_old, enableInt : std_logic := '0';										--This is used to detect only rising edges of INTRPS2
	signal interruptionPos : STD_LOGIC_VECTOR (2 DOWNTO 0):= (others => '0');		--Address of interruption
begin

process (clk, res)
begin
	if res = '1' then						--The registers are cleared in case of a reset
		IFLAGS <= (others => '0');
		IEN <= (others => '0');
		enableInt <= '0';
		ps2int_old <= '0';
	elsif rising_edge(clk) then
		if enREGS = '1' then			--Logic to store data in the registers
			if inSelRegs = "101" then
				IEN <= input; 			
			elsif inSelRegs = "110" then
				IFLAGS <= input;
			end if;
		end if;
		
		if INTRPS2 = '1' and ps2int_old = '0' then				--In case of a rising edge on the signal from the keyboard. 
			IFLAGS(1) <= '1';													--IGLAGS(0) is set
			ps2int_old <= '1';
		elsif INTRPS2 = '0' and ps2int_old = '1' then				--This is just some logic to handle only the rising edge of the signal
			ps2int_old <= '0';
		end if;
		
		if IEN(8) = '1' and IEN(1) = '1' and IFLAGS(1) = '1' then	--If an interruption happens	
			IEN(8) <= '0';															--Global enable is disabled
			IFLAGS(1) <= '0';														--Interruption flag is disabled
			enableInt <= '1';														--A signal is sent to controller
			interruptionPos <= "001";											--The address of the interruption is outputed
		end if;
		
		if GIEstate = '1' then													--Signal from controller when the interruption is over
			IEN(8) <= '1';															--Enable interruptions again
			enableInt <= '0';														--Outputs 0
		end if;
	end if;
end process;
		
enINTR <= enableInt;
handledIntrp <= interruptionPos;
outREGS <= "0000" & IEN when inSelRegs = "101" 
		else "0000" & IFLAGS when inSelRegs = "110"
		else (others => 'Z');
end Behavioral;
