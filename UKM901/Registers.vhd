library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Registers is
		Port ( input	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 			--Data input
			 inPSW : IN	STD_LOGIC_VECTOR (3 DOWNTO 0); 					--Input for PSW. ALU flags			
			 clk, res, enPC, enIR, enACC, enREGS, enPSW:	IN	STD_LOGIC;--Enable bits, interruption PS2 and enable GIE from Control
			 inSelRegs : in STD_LOGIC_VECTOR (2 DOWNTO 0);				--'Register to be outputed or updated'
			 outSelRegs : out STD_LOGIC_VECTOR (2 DOWNTO 0);			--'Interruption to be handled' & 'Register to update'
			 retType : out STD_LOGIC_VECTOR (1 DOWNTO 0);						--'type of return operation'
			 outOpCode, outAddrMode : out STD_LOGIC_VECTOR (3 DOWNTO 0);	--OPCODE and ADDRESSING_MODE
			 outIR, outPC  : out STD_LOGIC_VECTOR (11 DOWNTO 0);	--Outputs for PC, IR and one of the 6 other registers
          outREGS, outACC	:	OUT STD_LOGIC_VECTOR (15 DOWNTO 0));					--Output of ACC
end Registers; 
 
architecture Behavioral of Registers is
	signal regPC : std_logic_vector(11 downto 0) := (others => '0');			
	signal regIR, regACC : std_logic_vector(15 downto 0) := (others => '0');
	type REGARRAY is array (0 to 4) of std_logic_vector(15 downto 0);			
	signal REGS : REGARRAY := (others => (others => '0'));						--SP, PTR1, PTR2, PTR3, PSW
begin

process(clk, res) 	
	begin			--the registers will be updated according to the enable pin which is set.
		if res = '1' then									--We reset all the registers
			REGS <= (others => (others => '0'));
			regPC <= (others => '0');
			regIR <= (others => '0');
			regACC <= (others => '0');
		elsif rising_edge(clk) then
			if enPC = '1' then 							
				regPC <= input(11 downto 0); 						--The input goes to PC
			end if;
			if enACC = '1' then 							
				regACC <= input; 										--The input goes to ACC
			end if;
			if enIR = '1' then 
				regIR <= input; 										--The input goes to IR
			end if;
			if enREGS = '1' and (to_integer(unsigned(inSelRegs)) < 5) then 				 				
				REGS(to_integer(unsigned(inSelRegs))) <= input; --The input goes to the register which number corresponds to the inSelRegs signal content
			end if;
			if enPSW = '1' then
				REGS(4) <= X"000" & inPSW;							--We save OV C N Z
			end if;
		end if;
end process;

process(regPC, regIR, regACC)--, inSelRegs)		--If any signal is updated	   
begin					      											
	outPC <= regPC;					
	outACC <= regACC;
	outIR <= regIR(11 downto 0);
	retType <= regIR(9 downto 8);						--Bits containing the return type operation
	outOpCode <= regIR(15 downto 12);				--Bits containing the OPCODE
	outAddrMode <= regIR(7 downto 4);				--Bits containing the addr. mode
	outSelRegs <= regIR(2 downto 0);					--In case of STORER or LOADR, bits containing the register to update
end process;

outREGS <= REGS(0) when inSelRegs = "000" 		--The outputed register depends on inSelRegs
		else REGS(1) when inSelRegs = "001"
		else REGS(2) when inSelRegs = "010" 
		else REGS(3) when inSelRegs = "011" 
		else REGS(4) when inSelRegs = "100" 
		else (others => 'Z');

end Behavioral;
