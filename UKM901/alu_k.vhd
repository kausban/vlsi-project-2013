-- Group E milestone 1
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ALU is
  port (
     A, B                : in  std_logic_vector(15 downto 0);
     C                   : out std_logic_vector(15 downto 0);
     ALUfunc             : in  std_logic_vector(3 downto 0);
     ShifRot 	 			 : in  std_logic_vector(1 downto 0);
     n, z, cout, ov      : out std_logic );
end ALU;

architecture behaviour of ALU is

  begin
 		 
	alu_do : process(ALUfunc,A,B,ShifRot) -- depending on ALUFunc (opcode) do task
	variable temp_sum : std_logic_vector(16 downto 0) := "00000000000000000";		--17 word length sum to handle carry out
	begin
		ov <= '0';
		case ALUfunc is
			when "0000" => --set zero
				temp_sum := "00000000000000000";
			when "0001" => --add
				temp_sum := std_logic_vector(unsigned('0' & A) + unsigned('0' & B));
				if ((A(15) xnor B(15)) = '1' and (temp_sum(15) /= A(15))) then
					ov <= '1';  
				else  
					ov <= '0';
				end if;
			when "0010" => -- A-B
				temp_sum := std_logic_vector(unsigned('0' & A) + unsigned('0' & (not B)) + 1);
			when "0011" => -- B-A
				temp_sum := std_logic_vector(unsigned('0' & B) + unsigned('0' & (not A)) + 1);
			when "0100" => -- A
				if ShifRot = "11" then								--Rotate
					temp_sum := '0' & A(0) & A(15 downto 1);
				elsif ShifRot = "10" then							--Shift
					temp_sum := "00" & A(15 downto 1);
				else
					temp_sum := '0' & A;
				end if;
			when "0101" => -- B
				temp_sum := '0' & B;
			when "0110" => -- A + 1
				temp_sum := std_logic_vector(unsigned('0' & A) + 1);
				if (A(15) = '0' and temp_sum(15) = '1') then
					ov <= '1';  
				else  
					ov <= '0';
				end if;
			when "0111" => -- B + 1
				temp_sum := std_logic_vector(unsigned('0' & B) + 1);
				if (B(15) = '0' and temp_sum(15) = '1') then
					ov <= '1';  
				else  
					ov <= '0';
				end if;
			when "1000" => -- A and B
				temp_sum := '0' & (A and B);
			when "1001" => -- A or B
				temp_sum := '0' & (A or B);
			when "1010" => -- A xor B
				temp_sum := '0' & (A xor B);
			when "1011" => -- B - 1
				temp_sum := std_logic_vector(unsigned('0' & B) - 1);
			when "1100" => -- not A
				temp_sum := '0' & (not A);
			when "1101" => -- not B
				temp_sum := '0' & (not B);
			when "1110" => -- not A + 1
				temp_sum := std_logic_vector(unsigned('0' & (not A)) + 1);
				if ((not A(15)) = '0' and temp_sum(15) = '1') then
					ov <= '1';  
				else  
					ov <= '0';
				end if;
			when "1111" => -- not B + 1
				temp_sum := std_logic_vector(unsigned('0' & (not B)) + 1);
				if ((not B(15)) = '0' and temp_sum(15) = '1') then
					ov <= '1';  
				else  
					ov <= '0';
				end if;
			when others =>
				temp_sum := "00000000000000000";
		end case;
		C <= temp_sum(15 downto 0);						--Output
		cout <= temp_sum(16);								--Carry out
		n <= temp_sum(15);									--Sign
		if temp_sum(15 downto 0) = "0000000000000000" then --if zero
			z <= '1';
		else
			z <= '0';
		end if;
	end process;

end behaviour;
