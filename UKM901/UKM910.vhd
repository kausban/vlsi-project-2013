library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity UKM910 is
	Port ( oe	:	OUT	STD_LOGIC; 
          wren	:	OUT	STD_LOGIC; 		
          res	:	IN	STD_LOGIC; 
          clk	:	IN	STD_LOGIC; 
			 INTRPS2 : IN STD_LOGIC;
			 enLEDRes :	OUT	STD_LOGIC;
          addressbus	:	OUT	STD_LOGIC_VECTOR (11 DOWNTO 0); 
          databus	:	INOUT	STD_LOGIC_VECTOR (15 DOWNTO 0));
end UKM910;

architecture Behavioral of UKM910 is

	COMPONENT ALU
	PORT ( 
		  A, B                : in  std_logic_vector(15 downto 0);
		  C                   : out std_logic_vector(15 downto 0);
		  ALUfunc             : in  std_logic_vector(3 downto 0);
		  ShifRot 	 			 : in  std_logic_vector(1 downto 0);
		  n, z, cout, ov      : out std_logic );
	END COMPONENT;
	
	COMPONENT Multiplexer is
	PORT (inPC, inIR	  		:	IN	STD_LOGIC_VECTOR (11 DOWNTO 0); 
			inREGS, inDataBus, inACC		  		:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
			selAddr, selResult   		:  IN	STD_LOGIC_VECTOR (1 DOWNTO 0); 
			selB								:  IN	STD_LOGIC_VECTOR (2 DOWNTO 0); 
			handledIntrp:	IN STD_LOGIC_VECTOR (2 DOWNTO 0);
			outAdressBus     		  		:  OUT STD_LOGIC_VECTOR (11 DOWNTO 0);
			outBbus, outDataBus	  		:	OUT STD_LOGIC_VECTOR (15 DOWNTO 0));
	END COMPONENT;
	
	COMPONENT Registers is
	PORT ( input	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 			--Data input
			 inPSW : IN	STD_LOGIC_VECTOR (3 DOWNTO 0); 					--Input for PSW. ALU flags			
			 clk, res, enPC, enIR, enACC, enREGS, enPSW:	IN	STD_LOGIC; 	--Enable bits, interruption PS2 and enable GIE from Control
			 inSelRegs : in STD_LOGIC_VECTOR (2 DOWNTO 0);						--'Register to be outputed or updated'
			 outSelRegs : out STD_LOGIC_VECTOR (2 DOWNTO 0);	--'Interruption to be handled' & 'Register to update'
			 retType : out STD_LOGIC_VECTOR (1 DOWNTO 0);						--'type of return operation'
			 outOpCode, outAddrMode : out STD_LOGIC_VECTOR (3 DOWNTO 0);	--OPCODE and ADDRESSING_MODE
			 outIR, outPC   : out STD_LOGIC_VECTOR (11 DOWNTO 0);	--Outputs for PC, IR and one of the 6 other registers
          outACC, outREGS	:	OUT STD_LOGIC_VECTOR (15 DOWNTO 0));			--Output of ACC
	END COMPONENT;
	
	COMPONENT Interruptions is
	PORT	( input	:	IN	STD_LOGIC_VECTOR (11 DOWNTO 0); 	--Data input		
          res, clk, enREGS :	IN	STD_LOGIC; 					--Reset, clock, enable, signal from PS2 controller, signal from control
			 INTRPS2, GIEstate : IN	STD_LOGIC;
			 inSelRegs : IN STD_LOGIC_VECTOR (2 DOWNTO 0);	--'Register to be outputed or updated'
			 enINTR :	OUT	STD_LOGIC; 	
			 handledIntrp : OUT STD_LOGIC_VECTOR (2 DOWNTO 0);
			 outREGS  : OUT STD_LOGIC_VECTOR (15 DOWNTO 0));--Output for the IEN & IFLAGS registers
	END COMPONENT;
	
	type STATES is (FETCH1, FETCH2, PCINC, EXECUTE, STOP, NOP, READMEM, OPER, NOTROT, STORE, INITBRANCH, ENDBRANCH, JUMP, LOADR, STORER, LOADI, STOREI, INCDEC, CALL, PUSH, RET, RETGETPC, INCSP);
	signal state : STATES := STOP;
	
	signal Result_Bus, A_Bus, B_Bus, Regs_Bus, Regs : STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
	signal regPC, regIR : STD_LOGIC_VECTOR (11 DOWNTO 0) := (others => '0');
	signal OPCODE, ALUfunc, ADDRMODE : STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
	signal selAddr, selResult, RETTYPE, ShifRot : STD_LOGIC_VECTOR(1 downto 0) := "00";
	signal enRes, enREGS, enPSW, enPC, enIR, enACC, cout, z, n, ov, update_pc : STD_LOGIC := '0';
	signal selB, handledIntrp, inSelRegs, outSelRegs : STD_LOGIC_VECTOR (2 DOWNTO 0) := "000";
	signal GIEstate, enINTR, InterruptOn, Interrupting : STD_LOGIC := '0'; 
	
	begin

	comp_alu: ALU PORT MAP(
		ShifRot => ShifRot,
		cout => cout,
		z => z ,
		n => n,
		A => A_Bus,
		B => B_Bus,
		C => Result_Bus,
		ov => ov,
		ALUfunc => ALUfunc
   );

	comp_reg: Registers PORT MAP(
		input => Result_Bus,
		clk => clk,
		res => res,
		enPC => enPC,
		enREGS => enREGS,
		enPSW => enPSW,
		enIR => enIR,
		enACC => enACC,
		inPSW(0) => z,
		inPSW(1) => n,
		inPSW(2) => cout,
		inPSW(3) => ov,
		outOpCode => OPCODE,
		outAddrMode => ADDRMODE,
		outIR => regIR,
		outPC => regPC,
		outACC => A_Bus,
		outREGS => Regs,
		retType => RETTYPE,
		inSelRegs => inSelRegs,
		outSelRegs => outSelRegs
	);
	
	comp_intr : Interruptions PORT MAP(
		clk => clk,
		res => res,
		input => Result_Bus(11 downto 0),
		enREGS => enREGS,
		outREGS => Regs,
		handledIntrp => handledIntrp,
		INTRPS2 => INTRPS2,
		GIEstate => GIEstate,
		enINTR => enINTR,
		inSelRegs => inSelRegs
	);
	
	comp_mult: Multiplexer PORT MAP(
		inPC => regPC,
		inIR => regIR,
		inREGS => Regs,
		inACC => A_Bus,
		inDataBus => databus,
		handledIntrp => handledIntrp,
		selAddr => selAddr,
		selB => selB,
		selResult => selResult,
		outAdressBus => addressbus,
		outBbus => B_Bus,
		outDataBus => Regs_Bus
	);
	
	statecalc : process(clk, res)
	begin
	if res = '1' then
		state <= FETCH1;
		enPC <= '0';			--Disable PC
		oe <= '0';				--Disable reading from memory
		enACC <= '0';			--Disable writing in ACC
		enRes <= '0';			--Disable tristate Result Bus
		enIR <= '0';			--Disable Instruction register
		inSelRegs <= "000";	--SP will be outputed by default
		enPSW <= '0';
		enREGS <= '0';
		selResult <= "11";
		ShifRot <= "00";
		GIEstate <= '0';
		InterruptOn <= '0';
		Interrupting <= '0';
		enLEDRes <= '0';
	elsif rising_edge(clk) then
		enLEDRes <= '1';
		enPC <= '0';			--Disable PC
		oe <= '0';				--Disable reading from memory
		enACC <= '0';			--Disable writing in ACC
		enRes <= '0';			--Disable tristate Result Bus
		enIR <= '0';			--Disable Instruction register
		inSelRegs <= "000";	--SP will be outputed by default
		enREGS <= '0';
		enPSW <= '0';
		ALUfunc <= "0000";	--0
		ShifRot <= "00";
		selResult <= "11";	--ZZ
		GIEstate <= '0';
		case state is
			when FETCH1 =>	
				if enINTR = '1' and InterruptOn = '0' then		--If an interruption happened
					InterruptOn <= '1';
					Interrupting <= '1';			--for jump state
					state <= CALL;					
				else
					selAddr <= "01";			--Let PC register go to Address Bus
					oe <= '1';					--Enable reading from memory	
					state <= FETCH2;
				end if;
			when FETCH2 =>	
				selB <= "010";			--Let data on the bus go to B bus in ALU
				ALUfunc <= "0101";	--B goes though
				enIR <= '1';			--Enable Instruction register
				state <= PCINC;
			when PCINC =>
				selB <= "001";			--Let PC reg go to B bus
				ALUfunc <= "0111";	--B + 1 
				enPC <= '1';
				state <= EXECUTE;
			when EXECUTE =>
				case OPCODE is
					when "0000" =>								--NO OP
						state <= NOP;
					when "0001" | "0010" | "0011" | "1000" => 	--ADD, SUB, AND, LOAD
						state <= READMEM;
					when "0100" | "0101" | "0110" | "0111" =>		--NOT, COMP, ROT, SHIFT
						state <= NOTROT;
					when "1001" =>											--STORE
						state <= STORE;
					when "1100" =>											--JUMP
						state <= JUMP;
					when "1101" | "1110" =>								--BZ, BN
						state <= INITBRANCH;
					when "1010" =>											--LOAD-X / RETURN OPERATIONS
						if RETTYPE = "00"	then
							if ADDRMODE = "0000" then
								state <= LOADR;								--LOADR
							elsif ADDRMODE = "0011" then
								state <= INCDEC;								--LOADIDEC
							else
								state <= LOADI;								--LOADI / INC
							end if;
						else													--RET / RETI
							state <= RET;
						end if;
					when "1011" =>											--STORE-X
						if ADDRMODE = "0000" then
							state <= STORER;								--STORER
						elsif ADDRMODE = "0011" then					
							state <= INCDEC;								--STOREIDEC
						else
							state <= STOREI;				     			--STOREI /INC
						end if;
					when "1111" =>											--CALL
						state <= CALL;
					when others =>
						state <= STOP;
						null;
				end case;
			when NOP =>
				state <= FETCH1;
			when READMEM =>
				selAddr <= "00";		--Let IR go to Address Bus
				oe <= '1';				--Enable reading from memory
				state <= OPER;
			when OPER =>
				inSelRegs <= outSelRegs;
				selB <= "010";			--Let data on the bus go to B bus in ALU
				case OPCODE is
					when "0001" =>							--ADD
						ALUfunc <= "0001";				--ACC + ADDR							
					when "0010" =>							--SUB
						ALUfunc <= "0010";				--ACC - ADDR
					when "0011" =>							--AND
						ALUfunc <= "1000";				--ACC and ADDR
					when "1000" | "1010"  =>			--LOAD or LOADI
						ALUfunc <= "0101";				--OUT = B
					when others =>
						null;
				end case;
				enPSW <= '1';					--Save flags in PSW
				enACC <= '1';
				if OPCODE = "1010" and ADDRMODE = "0010" then			--if OP=LOADIINC
					state <= INCDEC;
				else
					state <= NOP;
				end if;
			when NOTROT =>
				case OPCODE is						
					when "0100" =>							--NOT ACC
						ALUfunc <= "1100";				--NOT A
					when "0101" =>							--NOT ACC + 1
						ALUfunc <= "1110";				--NOT A + 1
					when "0110" =>							--ROTATE RIGHT ACC
						ShifRot <= "11";					--Rot in ALU
						ALUfunc <= "0100";				--A
					when "0111" =>							--SHIFT RIGHT ACC
						ShifRot <= "10";					--Shift in ALU
						ALUfunc <= "0100";				--A
					when others =>
						null;
				end case;
				enPSW <= '1';					--Save flags in PSW
				enACC <= '1';
				state <= NOP;
			when STORE =>
				selResult <= "01";	--ACC goes to databus
				enRes <= '1';			--ACC is going to databus
				selAddr <= "00";		--Let IR go to Address Bus
				state <= FETCH1;
			when JUMP =>
				ALUfunc <= "0101";		--OUT = B
				if Interrupting = '1' then	--If an interruption is being handled
					selB <= "100";			--Address of interruption goes to B bus
					enPC <= '1';			--Save the address of interruption in PC
					Interrupting <= '0';					
					state <= FETCH1;		
				else
					selB <= "000";				--IR goes to ALU						
					enPC <= '1';
					state <= FETCH1;
				end if;
			when INITBRANCH =>
				ALUfunc <= "0100";		--A					
				state <= ENDBRANCH;
			when ENDBRANCH =>
				ALUfunc <= "0100";		--A
				if (OPCODE = "1101" and z = '1') or (OPCODE = "1110" and n = '1') then
					selB <= "000";						--LET IR go to ALU
					ALUfunc <= "0101";				--OUT = B
					enPC <= '1';
				end if;
				state <= FETCH1;	
			when STORER =>
				ALUfunc <= "0100";	--A
				enREGS <= '1';			--Enable writing in REGS
				inSelRegs <= outSelRegs;	--According to the code of IR
				state <= FETCH1;
			when LOADR =>					
				selB <= "011";				--Value on REGS goes to ALU
				inSelRegs <= outSelRegs;--According to the code of IR
				ALUfunc <= "0101";		--B
				enACC <= '1';				--Enable writing in ACC
				enPSW <= '1';			--Update PSW in next clock
				state <= FETCH1;
			when STOREI =>
				enRes <= '1';			--
				selAddr <= "10";		--Let REGS go to Address Bus
				selResult <= "01";	--acc goes to databus
				inSelRegs <= outSelRegs;--According to the code of IR
				if ADDRMODE = "0001" or ADDRMODE = "0011" then		--STOREI or STOREIDEC
					state <= FETCH1;
				else						--STOREIINC
					state <= INCDEC;
				end if;
			when LOADI =>
				selAddr <= "10";			--Value on REGS(IR) goes to Addr bus
				inSelRegs <= outSelRegs;--According to the code of IR
				oe <= '1';					--Enable reading from memory
				state <= OPER;
			when INCDEC =>	
				selB <= "011";					--Regs goes to B
				inSelRegs <= outSelRegs;	--According to the code of IR
				if ADDRMODE = "0010" then		--INC
					ALUfunc <= "0111";			--C = B+1
					enREGS <= '1';						
				elsif ADDRMODE = "0011" then	--DEC
					ALUfunc <= "1011";   		--C = B-1
					enREGS <= '1';	
				end if;
				if OPCODE = "1011" and ADDRMODE = "0011" then		--STOREIDEC operation
					state <= STOREI;
				elsif OPCODE = "1010" and ADDRMODE = "0011" then	--LOADIDEC operation
					state <= LOADI;	
				else
					state <= FETCH1;
				end if;
			when CALL =>
				selB <= "011";					--SP goes to B	/ SP by default
				ALUfunc <= "1011";   		--C = B-1
				enREGS <= '1';					--SP = SP-1
				state <= PUSH;
			when PUSH =>
				enRes <= '1';			--
				selResult <= "00";	--pc goes to databus
				selAddr <= "10";		--Let REGS go to Address Bus / SP by default				
				state <= JUMP;
			when RET =>
				selAddr <= "10";				--Value on SP goes to Addr bus / SP by default
				oe <= '1';						--Enable reading from memory
				state <= RETGETPC;
			when RETGETPC =>
				selB <= "010";					--Data Bus goes to B
				ALUfunc <= "0101";			--C = B
				enPC <= '1';					--Enable writting in PC
				state <= INCSP;
			when INCSP =>
				selB <= "011";					--SP goes to B	/ SP by default
				ALUfunc <= "0111";			--C = B+1
				enREGS <= '1';
				if RETTYPE = "10" then
					GIEstate <= '1';			--Finishes the interrupt
					InterruptOn <= '0';
					state <= NOP;
				else
					state <= FETCH1;
				end if;
			when others =>
				null;
		end case;
	end if;
	end process statecalc;
	
	output_tristate: process(enRES, Regs_Bus)
	begin
		if enRES = '1' then
			databus <= Regs_Bus;
		else
			databus <= (others => 'Z');
		end if;
	end process output_tristate;
	
	wren <= enRES;
end Behavioral;

