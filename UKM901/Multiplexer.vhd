library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--------------------------------------------------------------------
--This entity is just in charge of routing the registers to the ALU or to the Address Bus depending on to enable signals.
--------------------------------------------------------------------

entity Multiplexer is
	Port ( inPC, inIR 	  		  		:	IN	STD_LOGIC_VECTOR (11 DOWNTO 0); 
               inREGS, inDataBus, inACC		  		:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
               selAddr, selResult   		:  IN	STD_LOGIC_VECTOR (1 DOWNTO 0); 
					selB			:	IN STD_LOGIC_VECTOR (2 DOWNTO 0);
					handledIntrp:	IN STD_LOGIC_VECTOR (2 DOWNTO 0);
               outAdressBus     		  		:  OUT STD_LOGIC_VECTOR (11 DOWNTO 0);
               outBbus, outDataBus	  		:	OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
					);
end Multiplexer;

architecture Behavioral of Multiplexer is
begin
outBbus <= ("0000" & inIR) when selB = "000" 		--IR Goes to B bus
		else ("0000" & inPC) when selB = "001" 		--PC Goes to B bus
		else inDataBus when selB = "010" 				--Data Bus Goes to B bus
		else inREGS when selB = "011"		--REGS Goes to B bus
		else (X"000" & "0" & handledIntrp) when selB = "100"	--The address of the interruption being handled goes to B bus 
		else (others => 'Z'); 
		
outAdressBus <= inIR when selAddr = "00" 				--IR goes to address bus
		else inPC when selAddr = "01" 					--PC goes to address bus
		else inREGS(11 downto 0) when selAddr = "10" 					--REGS goes to address bus
		else (others => 'Z'); 

outDataBus <= ("0000" & inPC) when selResult = "00" 	--PC goes to tri-state buffer
		else inACC when selResult = "01" 					--ACC goes to tri-state buffer
		else inREGS when selResult = "10" 	--REGS goes to tri-state buffer
		else (others => 'Z'); 	
		
end Behavioral;


