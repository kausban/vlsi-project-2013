LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY UKM901_TB IS
END UKM901_TB;
ARCHITECTURE behavioral OF UKM901_TB IS 

   COMPONENT UKM910
   PORT(  oe	:	OUT	STD_LOGIC; 
          wren	:	OUT	STD_LOGIC; 		
          res	:	IN	STD_LOGIC; 
          clk	:	IN	STD_LOGIC; 
			 INTRPS2 : IN STD_LOGIC;
          addressbus	:	OUT	STD_LOGIC_VECTOR (11 DOWNTO 0); 
          databus	:	INOUT	STD_LOGIC_VECTOR (15 DOWNTO 0));
   END COMPONENT;

	COMPONENT memory is
	PORT ( 	clk : in  STD_LOGIC;
				addr, addrVGA : in  STD_LOGIC_VECTOR (11 downto 0);
				dataIO : inout  STD_LOGIC_VECTOR (15 downto 0);
				dataVGA : out  STD_LOGIC_VECTOR (7 downto 0);
				wren : in  STD_LOGIC;
				oe : in  STD_LOGIC);
	END COMPONENT;
	
	COMPONENT VGAdescribe is
	PORT (      clk                   : in std_logic;
					vsync, hsync, R, G, B         : out std_logic;
					Data  : in  std_logic_vector(7 downto 0) ;
					Address  : out  std_logic_vector (11 downto 0));
	END COMPONENT;
	
	COMPONENT PS2_Module is
	PORT(
			Clk 				: in std_logic;                       --System Clk
			Reset 			: in std_logic;                       --System Reset
			addr_bus		   : in std_logic_vector(11 downto 0);   --processor address bus
			PS2_Clk 			: in std_logic;                       --Keyboard Clock line
			PS2_Data 		: in std_logic;                       --Keyboard data line
			IRQ 				: out std_logic;                      --Interrupt Signal
			databus        : out std_logic_vector(15 downto 0);  --processor data bus
			OE					: in std_logic); 						  --processor enable	
	END COMPONENT;
	
	COMPONENT PS2Tester is
   PORT (
    PS2clk  : out std_logic;
    PS2data : out std_logic );
	END COMPONENT;
	
	COMPONENT vgaTesterDigInterface is
	PORT (
		 vsync : in std_logic;
		 hsync : in std_logic;
		 red   : in std_logic;
		 green : in std_logic;
		 blue  : in std_logic);
	END COMPONENT;

	signal res, clk, oe, wren : STD_LOGIC := '0';
	signal INTRPS2 : STD_LOGIC := '0';
	signal addressbus, addrVGA	:	STD_LOGIC_VECTOR (11 DOWNTO 0) := (others => '0');
	signal databus	:	STD_LOGIC_VECTOR (15 DOWNTO 0) := (others => '0');
	signal dataVGA	:	STD_LOGIC_VECTOR (7 DOWNTO 0) := (others => '0');
	
   signal vsync, hsync, R, G, B         :  std_logic; 
	signal Data  : std_logic_vector(7 downto 0) ;
	signal Address  :  std_logic_vector (11 downto 0);
	
	SIGNAL ps2clk	:	STD_LOGIC;
	SIGNAL ps2data	:	STD_LOGIC;

begin
   comp_ps2_tester : PS2Tester PORT MAP(
		PS2clk => ps2clk,
		PS2data => ps2data
	);
	
	comp_processor: UKM910 PORT MAP(
		oe => oe, 
		wren => wren, 
		res => res, 
		clk => clk, 
		INTRPS2 => INTRPS2,
		addressbus => addressbus, 
		databus => databus
   );

	comp_memory: memory PORT MAP(
		clk => clk,
		addr => addressbus,
		addrVGA => addrVGA,		
		dataIO => databus,
		dataVGA => dataVGA,
		wren => wren,
		oe => oe  
   );

	comp_VGA: VGAdescribe PORT MAP(
		clk => clk,
		vsync => vsync,
		hsync => hsync,
		R => R,
		B => B,
		G => G,
		Data => dataVGA,
		Address => addrVGA
   );
	
	comp_ps2 : PS2_Module PORT MAP(
		Clk => clk,
		Reset => res,
		addr_bus => addressbus,
		PS2_Clk => ps2clk,
		PS2_Data => ps2data,
		IRQ => INTRPS2,
		databus => databus,
		OE => oe
	);
	
	comp_tester: vgaTesterDigInterface PORT MAP(
		vsync => vsync,
		hsync => hsync,
		red => R,
		blue => B,
		green => G
   );
	
	createReset : PROCESS 
	BEGIN
		res <= '1';
		WAIT FOR 20 ns;
		res <= '0';
		WAIT;
	END PROCESS createReset;
	
	clock_driver : process     
   variable tmp_clk : std_logic := '1';
   	begin	
    		tmp_clk := not tmp_clk;
    		clk	<= tmp_clk;
  		wait for 10 ns;
   end process clock_driver;
	
--	
--process
--begin
--	INTRPS2<='0';
--	WAIT FOR 3000 ns;
--	INTRPS2<='1';
--	WAIT FOR 1000 ns;
--	INTRPS2<='0';
--	WAIT FOR 1000 ns;
--	INTRPS2<='1';
--	WAIT;
--end process;

END;


